const { mix, config } = require('laravel-mix');

config.publicPath = 'assets';

mix.js('resources/js/app.js', 'assets/js')
   .sass('resources/sass/app.scss', 'assets/css');

if (mix.inProduction()) {
    mix.version();
}

